import pytest
import redis
import requests_mock

from shadts.gdoc import RatingsTable
from .test_app import test_app
from .test_course import mock_deadlines
from .test_gdoc import ratings_table


def test_api_token(test_app):
    with test_app.test_client() as client:
        assert 403 == client.post("/api/report").status_code


def test_api(test_app, ratings_table):
    ratings_table.sync_task_columns(["foo", "zogzog"])

    with test_app.test_client() as client:
        with requests_mock.mock(real_http=True) as m:
            mock_deadlines(m)

            client.post("/api/sync_task_columns", data={
                "token": test_app.config["TESTER_TOKEN"],
            })                

            rsp = client.post("/api/report", data={
                "token": test_app.config["TESTER_TOKEN"],
                "user_id": 1,
                "task": "foo"
            })

            assert rsp.status_code == 200
            assert "30" == ratings_table.ws.range("H4")[0].value

            rsp = client.post("/api/report", data={
                "token": test_app.config["TESTER_TOKEN"],
                "user_id": 1,
                "task": "zogzog"
            })

            assert rsp.status_code == 200
            assert "200" == ratings_table.ws.range("I4")[0].value


def flush_redis(app):
    redis.StrictRedis(app.config["REDIS"]).flushall()


def test_limit_attempts(test_app, ratings_table):
    ratings_table.clear()
    ratings_table.sync_task_columns(["zog"])
    flush_redis(test_app)

    with test_app.test_client() as client:
        with requests_mock.mock(real_http=True) as m:
            mock_deadlines(m)

            for _ in range(2):
                rsp = client.post("/api/report", data={
                    "token": test_app.config["TESTER_TOKEN"],
                    "user_id": 1,
                    "task": "zog",
                    "failed": 1,
                })

                assert rsp.status_code == 200

            rsp = client.post("/api/report", data={
                "token": test_app.config["TESTER_TOKEN"],
                "user_id": 1,
                "task": "zog",
            })
            assert rsp.status_code == 200
            assert "30" == ratings_table.ws.range("H4")[0].value
