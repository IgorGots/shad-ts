import uuid
import logging
import urllib
import requests
import gitlab
import functools
import json

from flask import session, redirect, request, url_for, render_template

from . import app
from . import signup


logger = logging.getLogger(__name__)


def requires_auth(f):
    @functools.wraps(f)
    def decorated(*args, **kwargs):
        if "gitlab" not in session:
            return redirect(url_for("signup"))

        return f(*args, **kwargs)
    return decorated


@app.route("/login")
def login():
    if "LOCAL_SESSION" in app.config:
        session["gitlab"] = json.loads(app.config["LOCAL_SESSION"])
        session.permanent = True
        return redirect(url_for("main_page"))
    
    oauth_state = uuid.uuid4().hex
    session["oauth_state"] = oauth_state
    return redirect(app.config["GITLAB_URL"] + "/oauth/authorize?" + urllib.parse.urlencode({
        "client_id": app.config["GITLAB_CLIENT_ID"],
        "redirect_uri": request.host_url + "login_finish",
        "response_type": "code",
        "state": oauth_state,
    }))


class GitlabOAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self._token = token

    def __call__(self, r):
        r.headers["Authorization"] = "Bearer " + self._token
        return r


@app.route("/login_finish")
def login_finish():
    code = request.args["code"]
    state = request.args["state"]

    if "oauth_state" not in session or state != session["oauth_state"]:
        redirect(url_for("login"))
    del session["oauth_state"]

    auth_rsp = requests.post(app.config["GITLAB_URL"] + "/oauth/token", data={
        "client_id": app.config["GITLAB_CLIENT_ID"],
        "client_secret": app.config["GITLAB_CLIENT_SECRET"],
        "redirect_uri": request.host_url + "login_finish",
        "grant_type": "authorization_code",
        "code": code
    })

    auth_rsp.raise_for_status()
    gitlab_token = auth_rsp.json()["access_token"]

    user_rsp = requests.get(app.config["GITLAB_URL"] + "/api/v4/user", auth=GitlabOAuth(gitlab_token))
    user_rsp.raise_for_status()
    user = user_rsp.json()

    if "nocreate" not in request.args:
        try:
            signup.maybe_create_project_for_user(user["username"], user["id"])
        except gitlab.GitlabError as ex:
            logger.error("Project creation failed: {}".format(ex.error_message))
            return render_template("signup.html", error_message=ex.error_message)

    session["gitlab"] = {
        "token": gitlab_token,
        "username": user["username"],
        "id": user["id"],
        "is_admin": user.get("is_admin", False),
    }
    session.permanent = True

    return redirect(url_for("main_page"))


@app.route("/logout")
def logout():
    del session["gitlab"]
    return redirect(url_for("main_page"))
