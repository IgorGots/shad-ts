default:
	@echo "usage: make deploy"

I=eu.gcr.io/shad-ts/web
GSSH=gcloud compute --project shad-ts ssh web --

deploy:
	docker build . -t $(I)
	docker push $(I)
	$(GSSH) sudo gcloud docker -- pull $(I)
	$(GSSH) 'sudo docker kill $$(sudo docker ps -q)'
	$(GSSH) sudo docker run -d -v /etc/shadts/cpp.cfg:/etc/shadts.cfg -p 8080:8080 --restart always $(I)
	$(GSSH) sudo docker run -d -v /etc/shadts/os.cfg:/etc/shadts.cfg -p 8081:8080 --restart always $(I)
	$(GSSH) sudo docker run -d -v /etc/shadts/hse-programming-intro.cfg:/etc/shadts.cfg -p 8082:8080 --restart always $(I)
	$(GSSH) sudo docker run -d -v /etc/shadts/ds.cfg:/etc/shadts.cfg -p 8083:8080 --restart always $(I)

.PHONY: deploy default
